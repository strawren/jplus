/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus;

import io.jboot.utils.StrUtil;
import io.jplus.codegen.JplusModelGenerator;
import io.jplus.codegen.JplusServiceGenerator;
import io.jplus.codegen.JplusServiceImplGenerator;

public class CodeGen {

    public static void main(String[] args) {
        String modelPackage = "io.jplus.admin.model";
        String basePackage = "io.jplus.admin.service";

        //指定生成的表
        String tables = "schedule_job,schedule_job_log,sys_config,sys_dept,sys_dict,sys_log,sys_menu,sys_oss,sys_role, sys_role_dept,sys_role_menu,sys_user,sys_user_role";

        //要排除的表，主要是没有主键的表
        String[] excludeTables = {};
        JplusModelGenerator.runWith(modelPackage, tables, StrUtil.join(excludeTables, ","), "sys_");
        JplusServiceGenerator.runWith(basePackage, modelPackage, tables, StrUtil.join(excludeTables, ","), "sys_");
        JplusServiceImplGenerator.runWith(basePackage, modelPackage, tables, StrUtil.join(excludeTables, ","), "sys_");

        //默认生成全部
        //JplusModelGenerator.run(modelPackage,"sys_");
        //JplusServiceGenerator.run(basePackage, modelPackage,"sys_");
    }

}
