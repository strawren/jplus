package io.jplus.admin.service;

import com.jfinal.plugin.activerecord.Page;
import io.jboot.db.model.Column;
import io.jboot.db.model.Columns;
import io.jplus.admin.model.Role;
import io.jplus.common.Query;

import java.util.List;

public interface RoleService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public Role findById(Object id);


    /**
     * find all model
     *
     * @return all <Role
     */
    public List<Role> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(Role model);


    /**
     * save model to database
     *
     * @param model
     * @return id value if save success
     */
    public Object save(Role model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if save or update success
     */
    public Object saveOrUpdate(Role model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(Role model);


    /**
     * page query
     *
     * @param page
     * @param pageSize
     * @return page data
     */
    public Page<Role> paginate(int page, int pageSize);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @return page data
     */
    public Page<Role> paginateByColumns(int page, int pageSize, Columns columns);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @param orderBy
     * @return page data
     */
    public Page<Role> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);

    public List<Role> findByColumn(Column column);

    public List<Role> findByUserId(Long userId);

    Page<Role> queryPage(Query query);

    boolean saveRole(Role role);

    boolean updateRole(Role role);

    Role findRoleById(Object id);

    public boolean deleteById(Object... ids);
}